	(function(window) {
	function Player(imgPlayer){
		this.initialize(imgPlayer);
	}
	Player.isIdle = true;
	Player.groundedAnim;
	Player.direction = 1;
	Player.prototype = new createjs.Sprite();
	Player.prototype.Sprite_initialize = Player.prototype.initialize;
	Player.prototype.initialize = function (imgPlayer){
			this.velocity = {x:0,y:12};
			var playerSpriteSheet = new createjs.SpriteSheet({
			// Sprite sheet stuff
				images: [imgPlayer],
				frames: [
					[0,0,26,26], //beginWalk0
					[26,0,26,26], //walk0
					[52,0,26,26], //walk1
					[78,0,26,26], //walk2
					[0,26,26,26], //stand0
					[26,26,26,26], //stand1
					[0,52,28,32], //jump0
				
				],
				animations: {
					stand:{
						frames:[4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,4,5],
						speed:0.3
					},
					
					walk:{
						frames:[1,2,3],
						next:"walk",
						speed:0.3
					},
					beginWalk:{
						frames:[0],
						next:"walk",
					},
					
					jump:{
						frames:[6],
					},
				}
			});
			
		this.constructor(playerSpriteSheet);
	Player.prototype.tick = function () {
		this.velocity.y += 1;
		
		 // preparing the variables
		var c = 0,
			cc = 0,
			addY = this.velocity.y,
			addX = this.velocity.y,
			bounds = getBounds(this),
			cbounds,
			collision = null,
			Hcollision = null,
			collideables = getCollideables();
	 
			cc=0;
		// for each collideable object we will calculate the
		// bounding-rectangle and then check for an intersection
		// of the hero's future position's bounding-rectangle
		while ( !collision && cc < collideables.length ) {
				cbounds = getBounds(collideables[cc]);
				if ( collideables[cc].isVisible ) {
					collision = calculateIntersection(bounds, cbounds, 0, addY);
				}
	 
				if ( !collision && collideables[cc].isVisible ) {
					// if there was NO collision detected, but somehow
					// the hero got onto the "other side" of an object (high velocity e.g.),
					// then we will detect this here, and adjust the velocity according to
					// it to prevent the Hero from "ghosting" through objects
					// try messing with the 'this.velocity = {x:0,y:25};'
					// -&gt; it should still collide even with very high values
					if ( ( bounds.y < cbounds.y && bounds.y + addY > cbounds.y )
					  || ( bounds.y > cbounds.y && bounds.y + addY < cbounds.y ) ) {
						addY = cbounds.y - bounds.y;
						addX = cbounds.x - bounds.x;
					} else {
						cc++;
					}
				}
			}
				
		 
		// if no collision was to be found, just
		// move the hero to it's new position
		if ( !collision ) {
			this.y += addY;
			if ( this.onGround ) {
				this.onGround = false;
			}
		// else move the hero as far as possible
		// and then make it stop and tell the
		// game, that the hero is now "an the ground"
		} else {
			this.y += addY - collision.height;
			if ( addY > 0 ) {
				this.onGround = true;
			}
			this.velocity.y = 0;
			
		}
		this.walk();
		// If player touches the ground switch back to the walking animation
		// Make sure to set a flag here to that the animation is played from beginning each time!
		
		if(this.onGround && this.groundedAnim==false){
			if(this.velocity.x == 3){
				this.gotoAndPlay("beginWalk");
			}else{
			// Set flag here! maybe like after jump
			this.gotoAndPlay("stand");
			}
			this.groundedAnim = true;  
		}
	}
		
		Player.prototype.jump = function() {
			// if the hero is "on the ground"
			// let him jump, physically correct!
			if ( this.onGround ) {
				this.velocity.y = -9;
				this.onGround = false;
			}
		}
		
		Player.prototype.walk = function(){
				// Move sprite based on velocity
                this.x += this.velocity.x * this.direction;
		}
		
		Player.prototype.setFlip = function(x,y,flip){
		// Minor function used to set the center of an animation since the frames are of different size
		// Also flips horizontally
			if(flip){
			this.scaleX = -1;
			this.direction = -1;
			}else{
			this.scaleX = 1;
			this.direction = 1;
			}
			this.regX = x;
			this.regY = y;
		}
		

	}
	window.Player = Player;
	} (window));
	